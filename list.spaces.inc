<?php
  /**
 * Implementation of hook_context_ui_define().
 */
function list_context_ui_define() {
  $items = array();
  $items[] = array(
    'namespace' => 'spaces',
    'attribute' => 'feature',
    'value' => 'list',
    'node' => array('list'),
    'spaces' => array(
      'label' => t('Task List'),
      'description' => t('A Task List to maintain record of your tasks.'),
      'menu' => array(
        'list' => array('title' => t('Task List')),
      ),
      'types' => array('og', 'site'),
      'settings' => array(
      'list' => new list_space_setting('list'),
      ),
    ),
    'block' => array(
      array(
        'module' => 'list',
        'delta' => 'spaces',
        'region' => 'right',
        'weight' => -11,
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_context_ui_active_contexts_alter().
 */
function list_context_ui_active_contexts_alter(&$contexts) {
  $space = spaces_get_space();
  if ($space && $space->feature_access('list') && $feature = context_get('spaces', 'feature')) {
    $display = !empty($space->settings['list']['display']) ? !empty($space->settings['list']['display'][$feature]) : TRUE;
    $position = !empty($space->settings['list']['position']) ? $space->settings['list']['position'] : 'bottom';
    $weight = ($position == 'bottom') ? 20 : -20;
    if ($display) {
      $contexts['spaces:feature:'. $feature]->block['list-spaces'] = (object) array(
        'module' => 'list',
        'delta' => 'spaces',
        'weight' => $weight,
        'region' => 'right',
        'bid' => $bid,
      );
    }
  }
}

/**
 * Implementation of space_setting.
 */
class list_space_setting implements space_setting {
  var $id;
  
  function __construct($id = NULL) {
    $this->id = isset($id) ? $id : 'list';
  }
  
  function form($space, $value = array()) {
    $form = array();
    $options = array();

    $features = spaces_features($space->type);
    foreach ($features as $id => $feature) {
      if ($space->feature_access($id) && $id !== 'list') {
        $options[$id] = $feature->spaces['label'];
      }
    }

    $form['display'] = array(
      '#title' => t('Display'),
      '#type' => 'checkboxes',
      '#description' => t('Choose which feature pages the Task Lisk should be displayed on.'),
      '#options' => $options,
      '#default_value' => !empty($value['display']) ? $value['display'] : array_keys($options),
    );

    $form['position'] = array(
      '#title' => t('Position'),
      '#type' => 'select',
      '#description' => t('Choose where to display the Task List.'),
      '#options' => array('top' => t('Top'), 'bottom' => t('Bottom')),
      '#default_value' => !empty($value['position']) ? $value['position'] : 'bottom',
    );

    return $form;
  }

  function validate($space, $value) {
  }

  function submit($space, $value) {
    return $value;
  }
}