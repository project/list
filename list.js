
/**
 * list class.
 */
var List = {
  /**
   * Nub Timer class.
   */
  NubTimer: {
    tid: 0, // Timer ID

    /**
     * Stop the Nub Timer.
     */
    stop: function() {
      if (this.tid) {
        clearTimeout(this.tid); // stop timer
        this.tid = 0;
      }
    },

    /**
     * Execute the Nub Timer.
     */
    exec: function() {
      $('.list .nub').css('display', 'none');
      $(this).stop();
    },

    /**
     * Start the Nub Timer.
     */
    start: function() {
      this.stop();
      this.tid = setTimeout(this.exec, 300); // start timer
    }
  },

  add_incomplete_list: function() {
    if (!$('.list.incomplete').length) {
      $('<ul class="list incomplete"></ul>').appendTo('#list-add-task-form ~ div.item-list:first');
    }

    // create one nub for each incomplete list item
    $('.list.incomplete .task').each(List.add_nub);

      // make every incomplete list sortable
     $('.list.incomplete').sortable({
       handle: '.reorder',
       items: 'li',
       revert: true,
       update: List.incomplete_list_reorder,
       opacity: 0.5,
       zIndex: 999999
     });
  },

  incomplete_list_reorder: function() {
    var serial = $('.list.incomplete').sortable('serialize');
    $.ajax({
      type: 'POST',
      url: Drupal.settings.basePath + 'index.php?q=list/reorder_task',
      data: serial
    });
  },

  /**
   * Bind event hooks to complete/uncomplete a task.
   */
  bind_task: function() {
    $('input[@type=checkbox]', this).click(function() {
      var element = $(this).parent();
      $.ajax({
        type: 'POST',
        url: Drupal.settings.basePath + 'index.php?q=list/toggle_task',
        data: 'id='+this.name+'&checked='+(this.checked? '1' : '0')+'&sort='+(this.checked? -1 : $('.list.incomplete .task').length+1),
        dataType: 'script'
      });
    });
  },

  delete_task: function(id) {
    var element = $('#'+id);
    $.ajax({
      type: 'POST',
      url: Drupal.settings.basePath + 'index.php?q=list/delete_task',
      data: 'id='+id,
      dataType: 'script'
    });
    return false;
  },

  edit_task: function(id) {
    var element = '';
    if (id instanceof Object) {
      element = $(this).parent();
      $.ajax({
        type: 'POST',
        url: Drupal.settings.basePath + 'index.php?q=list/edit_task',
        data: $('input, textarea, select', this).serialize(),
        dataType: 'script'
      });
    } else {
      element = $('#'+id);
      $.ajax({
        type: 'GET',
        url: Drupal.settings.basePath + 'index.php?q=list/edit_task&id='+id,
        dataType: 'script'
      });
    }
    return false;
  },

  /**
   * Create one nub for each incomplete list item.
   */
  add_nub: function() {
    if (!$('.nub', this).length) {
      $(this).prepend(
        '<div class="nub" style="display:none">' +
        '  <a href="javascript:void(0)" title="Delete this task" class="delete" onclick="return List.delete_task(\''+ $('input[@type=checkbox]', this).attr('name') +'\')">Delete</a>' +
        '  <a href="javascript:void(0)" title="Edit this task" class="edit" onclick="return List.edit_task(\''+ $('input[@type=checkbox]', this).attr('name') +'\')">Edit</a>' +
        '  <a href="javascript:void(0)" title="Reorder this task" class="reorder">Reorder</a>' +
        '</div>').each(List.bind_nub);
    }
  },

  /**
   * Bind event hooks to hide/show the nub.
   */
  bind_nub: function() {
    if (!$('.nub', this).length) {
      return $(this).each(List.add_nub);
    }

    $(this).mouseover(function() {
      with ($('.nub', this)) {
        List.NubTimer.exec(); // hide all other nubs
        css('display', 'block'); // must render before offsetWidth can be calculated
        css('left', (get(0).offsetWidth*-1)+'px');
        css('top', '-4px');
      }

      List.NubTimer.stop();
    });
    $(this).mouseout(function() { List.NubTimer.start(); });
    $('.nub', this).mouseover(function() { List.NubTimer.stop(); });
    $('.nub', this).mouseout(function() { List.NubTimer.start(); });
  },

  /**
   * Unbind event hooks to hide/show the nub.
   */
  unbind_nub: function() {
    $(this).unbind('mouseover');
    $(this).unbind('mouseout');
    $('.nub', this).unbind('mouseover');
    $('.nub', this).unbind('mouseout');
    List.NubTimer.exec(); // hide all nubs
  }
};

$(function() {
  List.add_incomplete_list();

  $.ajaxSetup({
    error: function(xml, status, e) {
      if (status == 'error' && e) {
        alert('An exception occurred in the script. Error name: ' + e.name + '. Error message: ' + e.message);
      } else if (xml.status === 0) {
        // retry
        $.ajax({
          type: this.type,
          url: this.url,
          data: this.data,
          dataType: this.dataType
        });
      }
      else {
        alert('Unknown AJAX Error: ' + status + ' Status:' + xml.status + ' ' + xml.statusText);
      }
    }
  });

  $('#list-add-task-form').submit(function() {
    $('input#edit-task', this).val($.trim($('input#edit-task', this).val()));
    if ($('input', this).val() === '') {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: Drupal.settings.basePath + 'index.php?q=list/create_task',
      data: $('input, select', this).serialize() + '&sort='+($('.list.incomplete .task').length+1),
      dataType: 'script'
    });
    this.reset();
    return false;
  });

  // Bind event hooks to complete/uncomplete a task.
  $('.list .task').each(List.bind_task);
});

/*
function getBaseUrl() {
  var url = '';
  var script = $.grep($('head > script'), function(i) {
    return $(i).attr('src').indexOf('list.js') > -1
  });
  url = $(script).attr('src');
  url = url.substring(0, url.indexOf('modules/'));
  if (url.indexOf('sites/') > -1)
    url = url.substring(0, url.indexOf('sites/'));
  return url;
}
*/
