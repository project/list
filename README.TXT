
LICENSE:
--------
All modules hosted by Drupal.org are automatically licensed under the
popular open-source GNU Public License (GPL). Therefore, this software
is provided "as is" without express or implied warranty of any kind.
For details, please see:

  Why drupal.org doesn't host GPL-"compatible" code
  http://drupal.org/node/66113


INSTALLATION:
-------------
Successful installation of this module requires nothing out of the ordinary.
If this is your first time installing a Drupal module, please see:

  Installing contributed modules
  http://drupal.org/node/70151


FEEDBACK AND SUPPORT:
--------------------
If you have any questions, suggestions, bug reports, or feature requests,
you may submit them to Drupal.org where they may be carefully reviewed
and addressed by volunteers.

  list
  http://drupal.org/project/list



